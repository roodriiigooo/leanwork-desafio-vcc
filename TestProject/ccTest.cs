﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace TestProject
{
    
    
    /// <summary>
    ///This is a test class for ccTest and is intended
    ///to contain all ccTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ccTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for TestaCc
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\leanwork\\leanwork-desafio-vcc", "/leanwork-desafio-vcc")]
        [UrlToTest("http://localhost/leanwork-desafio-vcc")]
        public void TestaCcTest()
        {
            string numeroCc = "9111111111111111"; 
            bool esperado = false; 
            bool atual;
            atual = cc_Accessor.TestaCc(numeroCc);
            Assert.AreEqual(esperado, atual);
        }

        /// <summary>
        ///A test for TipoCc
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\leanwork\\leanwork-desafio-vcc", "/leanwork-desafio-vcc")]
        [UrlToTest("http://localhost/leanwork-desafio-vcc")]
        public void TipoCcTest()
        {
            string numeroCc = "0"; 
            string esperado = "Desconhecido";
            string atual;
            atual = cc_Accessor.TipoCc(numeroCc);
            Assert.AreEqual(esperado, atual);
        }
    }
}
