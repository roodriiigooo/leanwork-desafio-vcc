﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class cc
{
	public cc()
	{

	}

    public static string Inverte(string s)
    {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }

    public static bool TestaCc(string numeroCc) 
    {
        int soma = 0;
        numeroCc = Inverte(numeroCc);

        for (int i = 0; i <= numeroCc.Length -1; i++)
        {
            int digito = int.Parse(numeroCc[i].ToString());
            if ((i % 2) == 0)
            {
                soma += digito;
            }
            else
            {
                soma += (2 * digito);
                if (digito >= 5) 
                { 
                    soma -= 9; 
                }
            }
        }
        return ((soma % 10) == 0);
    }

    public static string TipoCc(string numeroCc) 
    {
        int tNumeroCartao = numeroCc.Length;
        String digitos = "";
        
        //Visa
        digitos = numeroCc.Substring(0, 1);
        if ((tNumeroCartao == 13 || tNumeroCartao == 16) && (digitos == "4"))
        {
            return "VISA";
        }
        //AMEX
        digitos = numeroCc.Substring(0, 2);
        if(tNumeroCartao == 15 && (digitos == "37" || digitos == "34"))
        {
            return "AMEX"; 
        }
        //MasterCard
        digitos = numeroCc.Substring(0, 2);
        if ((tNumeroCartao == 16) && (digitos == "51" || digitos == "55"))
        {
            return "MasterCard";
        }
        //Discover
        digitos = numeroCc.Substring(0, 4);
        if ((tNumeroCartao == 16) && (digitos == "6011"))
        {
            return "Discover";
        }

        return "Desconhecido";
    }
}