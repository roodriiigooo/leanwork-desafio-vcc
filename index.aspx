﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Verificador de cartão de crédito</title>
</head>
<body>
    <form id="form1" runat="server" style="border: 1px solid black; width: 320px; padding: 5px; box-shadow: 5px 5px 5px rgba(0,0,0,0.2);">
        <div style="padding: 2px; background-color: Gray">
            <strong style="padding-left: 2px; color:White;">Verificador de cartão de crédito</strong>
        </div>
        <div style="display:inline-block; padding:5px 0px 0 12px">
                <asp:Label ID="lblNumeroCartao" runat="server" Text="Número do cartão: "></asp:Label>
                <asp:TextBox ID="txtNumero" runat="server"></asp:TextBox><br />
                <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
                <br /><br />
        </div>
        <div style="border: 1px solid black; width: 307px; padding: 5px;">
                <div style="display:inline-block; padding:0px;">
                    <asp:Button ID="btnValidar" runat="server" Text="Validar" onclick="btnValidar_Click" style="width: 306px;" />
                </div>
        </div>
    </form>
        <br />
    <div style="border: 1px solid black; width: 320px; padding: 5px; box-shadow: 5px 5px 5px rgba(0,0,0,0.2);">
         <div style="padding: 2px; background-color: Gray">
              <strong style="padding-left: 2px; color:White;">Para os seguintes cartões de crédito:</strong>
         </div>
            <div style="display:inline-block; padding:5px 0px 0 12px">
                <ul style="list-style:none;">
                    <li>4111111111111111</li>
                    <li>4111111111111</li>
                    <li>4012888888881881</li>
                    <li>378282246310005</li>
                    <li>6011111111111117</li>
                    <li>5105105105105100</li>
                    <li>5105 1051 0510 5106</li>
                    <li>9111111111111111</li>
                </ul>
            </div>
        </div>
       <br />
    <div style="border: 1px solid black; width: 320px; padding: 5px; box-shadow: 5px 5px 5px rgba(0,0,0,0.2);">
         <div style="padding: 2px; background-color: Gray">
              <strong style="padding-left: 2px; color:White;">Espera-se o seguintes resultados:</strong>
         </div>
            <div style="display:inline-block; padding:5px 0px 0 12px">
                <ul style="list-style:none; padding-left: 0px;">
                    <li>VISA: 4111111111111111 (válido)</li>
                    <li>VISA: 4111111111111 (inválido)</li>
                    <li>VISA: 4012888888881881 (válido)</li>
                    <li>AMEX: 378282246310005 (válido)</li>
                    <li>Discover: 6011111111111117 (válido)</li>
                    <li>MasterCard: 5105105105105100 (válido)</li>
                    <li>MasterCard: 5105105105105106 (inválido)</li>
                    <li>Desconhecido: 9111111111111111 (inválido)</li>
                </ul>
            </div>
        </div>
</body>
</html>
